<?php
	$host = "localhost";
	$database = "message_app";
	$username = "root";
	$password = "";
	 
	try {
	    $con = new PDO("mysql:host={$host};dbname={$database}", $username, $password);
	}
	catch(PDOException $exception){
	    echo "Connection error: " . $exception->getMessage();
	}
?>