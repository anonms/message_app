<?php
include 'config/database.php';
 
$query = "SELECT id, name, message, date FROM messages";
$stmt = $con->prepare($query);
$stmt->execute();
$num = $stmt->rowCount();
 
if($num > 0){
    echo '<button id="new-message" class="btn btn-success" type="button" data-toggle="modal" data-target="#add-modal">New Message</button><br><br>';
    echo "<table class='table table-bordered table-hover'>";
        echo "<tr>";
            echo "<th class='width-30-pct'>Name</th>";
            echo "<th class='width-30-pct'>Message</th>";
            echo "<th>Date</th>";
            echo "<th style='text-align:center;'>Actions</th>";
        echo "</tr>";

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            echo "<tr>";
                echo "<td>{$name}</td>";
                echo "<td>{$message}</td>";
                echo "<td>{$date}</td>";
                echo "<td style='text-align:center;'>";
                    echo "<div class='message-id hidden'>{$id}</div>";
                    echo "<div class='edit-btn btn btn-success glyphicon glyphicon-edit'>"; // edit button
                    echo "</div>";
                    echo " ";
                    echo "<div class='delete-btn btn btn-danger glyphicon glyphicon-trash'>"; // delete button
                    echo "</div>";
                echo "</td>";
            echo "</tr>";
        }
    echo "</table>";
}

else{
    echo '<button id="new-message" class="btn btn-success" type="button" data-toggle="modal" data-target="#add-modal">New Message</button><br><br>';
    echo "<div class='noneFound'>No records found</div>";
}
?>