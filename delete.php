<?php
include_once 'config/database.php';
 
try {
    $query = "DELETE FROM messages WHERE id = ?";
    $stmt = $con->prepare($query);
     
    $stmt->bindParam(1, $_POST['id']);

    if($stmt->execute()){
        echo "Message deleted";
    }else{
        echo "Could not delete message";
    }
}
 
catch(PDOException $exception){
    echo "Error: " . $exception->getMessage();
}
?>