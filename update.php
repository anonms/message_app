<?php
include_once 'config/database.php';
date_default_timezone_set('Asia/Singapore');
try{
    $query = "UPDATE messages SET name=:name, message=:message, date=:date WHERE id=:id";
 
    $stmt = $con->prepare($query);
 
    $name=htmlspecialchars(strip_tags($_POST['name']));
    $message=htmlspecialchars(strip_tags($_POST['message']));
    $date=date("Y-m-d-"." "."h-i-s");
    $id=htmlspecialchars(strip_tags($_POST['id']));
 
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':message', $message);
    $stmt->bindParam(':date', $date);
    $stmt->bindParam(':id', $id);
 
    if($stmt->execute()){
        echo "Message updated";
    }else{
        echo "Unable to update message";
    }
}
catch(PDOException $exception){
    echo "Error: " . $exception->getMessage();
}
?>