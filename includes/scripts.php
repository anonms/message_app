<script src="public/js/jquery.js"></script>
<script src="public/js/bootstrap.min.js"></script>
<script type='text/javascript'>
    $(document).ready(function(){
        show(); //Call function to show records
        /**
         * Changes the title of the page
         */
        function changePageTitle(page_title){   
            $('#page-title').text(page_title);
            document.title=page_title;
        }
        /**
         * Shows the listing
         */
        function show(){
            $('#records').fadeOut('slow', function(){
                $('#records').load('show.php', function(){
                    $('#records').fadeIn('slow');
                });
            });
        }
        /**
         * Create a message
         */
        $(document).on('submit', '#new-message-form', function() {
            // post the data from the form
            $.post("create.php", $(this).serialize())
                .done(function(data) {
                    show();
                });
            return false;
        });
        /**
         * Show edit form
         */
        $(document).on('click', '.edit-btn', function(){ 
            changePageTitle('Update Message');
            var messageId = $(this).closest('td').find('.message-id').text();
            $('#records').fadeOut('slow', function(){
                $('#records').load('update_form.php?id=' + messageId, function(){
                    $('#records').fadeIn('slow');
                    $('#new-message').hide();
                });
            });
        });
        /**
         * Update the message
         */
        $(document).on('submit', '#update-message-form', function() {
	    $.post("update.php", $(this).serialize())
	        .done(function(data) {
	            show();
	        });
		});
		/**
		 * Delete a message
		 */
		$(document).on('click', '.delete-btn', function(){ 
		    if(confirm('Confirm delete')){
		        var messageId = $(this).closest('td').find('.message-id').text();
		        $.post("delete.php", { id: messageId })
		            .done(function(data){
		                console.log(data);
		                show();
		        });
		    }
		});
     // End of script
    });
 </script>

