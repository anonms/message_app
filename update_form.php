<?php
    include 'config/database.php';

    $messageId=isset($_GET['id']) ? $_GET['id'] : die('ERROR: Message ID not found');

    $query = "SELECT id, name, message, date FROM messages WHERE id = ? LIMIT 0,1";

    $stmt = $con->prepare($query);

    $stmt->bindParam(1, $messageId);

    if($stmt->execute()){

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $id = $row['id'];
        $name = $row['name'];
        $message = $row['message'];

    }
    else{
    echo "Unable to read record.";
    }
?>
<div class="col-md-10 col-md-offset-1">
    <form id="update-message-form" class="form-horizontal" method="post">
        <input type='hidden' name='id' value='<?php echo $id ;?>'/>
        <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-8">
                <input type='text' name='name' class='form-control' value='<?php echo htmlspecialchars($name, ENT_QUOTES); ?>' required />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Message</label>
            <div class="col-sm-8">
                <textarea name='message' class='form-control' required><?php echo htmlspecialchars($message, ENT_QUOTES); ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" class="btn btn-success">Save Changes</button>
                <button type="button" onClick="show()" class="btn btn-default">Return</button>
            </div>
        </div><br>
    </form>
</div>
<script type='text/javascript'>
    function show(){
        $('#records').fadeOut('slow', function(){
            $('#records').load('show.php', function(){
                $('#records').fadeIn('slow');
            });
        });
    }
</script>

