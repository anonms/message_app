<?php
include_once 'config/database.php';
 
try{
    $query = "INSERT INTO messages (name, message) VALUES (:name, :message)";
 
    $stmt = $con->prepare($query);
 
    $name=htmlspecialchars(strip_tags($_POST['name']));
    $message=htmlspecialchars(strip_tags($_POST['message']));
    var_dump($name);
    var_dump($message);

    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':message', $message);
 
    if($stmt->execute()){
        echo "Inserted a record";
    }else{
        echo "Failed to insert a record";
    }
}

catch(PDOException $exception){
    echo "Error: " . $exception->getMessage();
}
?>