<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Messaging App</title>
    <!-- Stylesheets -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
    <h1 class="text-center">Messaging App</h1>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div id="records"></div>
            </div>
        </div>
    </div>
    <!-- Add message modal -->
    <div class="modal fade" tabindex="-1" id="add-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">New Message</h4>
                </div>
                <div class="modal-body">
                    <!-- Add message form -->
                    <form id="new-message-form" method="post">
                        <div class="form-group">
                            <label>Your Name</label>
                            <input name="name" type="text" class="form-control" placeholder="Name" required>
                        </div>
                        <div class="form-group">
                            <label>Your Message</label>
                            <textarea name="message" style="min-height:256px; width: 100%" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- Scripts -->
<?php include 'includes/scripts.php';?>
</body>
</html>